<?php

/**
 * @file
 * Schema for FileMaker module.
 */


/**
 * Implementation of hook_schema().
 */
function filemaker_schema() {

  $schema['filemaker_web_layout'] = array(
    'description' => t("Node type that can interact with a FileMaker layout."),
    'fields' => array(
      'fmid' => array(
        'description' => t('Unique auto incrementing id.'),
        'type' => 'serial',
        'unsigned' => true,
        'not null' => true,
      ),
      'nid' => array(
        'type' => 'int',
        'unsigned' => true,
        'not null' => true,
        'default' => 0,
        'description' => t("The FileMaker node's {fmwebform}.nid"),
      ),
      'fmcid' => array(
        'type' => 'int',
        'unsigned' => true,
        'not null' => true,
        'default' => 0,
        'description' => t("The FileMaker connection's {filemaker_connection}.fmcid"),
      ),
      'layout' => array(
        'type' => 'text',
        'not null' => false,
        'description' => t("The name of the FileMaker layout that this webform will interact with."),
      ),
      'drupal_view' => array(
        'type' => 'text',
        'not null' => true,
        'description' => t("The FileMaker feature to include on the standard Drupal view."),
      ),
      'show_record_number' => array(
        'type' => 'int', 
        'unsigned' => true, 
        'not null' => false, 
        'default' => 1, 
        'description' => t("The FileMaker node's \"Show Record #\" setting")
      ),
      'records_per_page' => array(
        'type' => 'int', 
        'unsigned' => true, 
        'not null' => true, 
        'default' => 50, 
        'description' => t("The number for FileMaker records to return for each page request (pagination).")
      ),
    ),
    'primary key' => array('fmid'),
    'unique keys' => array(
      'nid' => array('nid')
    ),
  );

  $schema['filemaker_connection'] = array(
    'description' => t('Stores information to connect to a FileMaker database.'),
    'fields' => array(
      'fmcid' => array(
        'description' => t('Unique auto incrementing id.'),
        'type' => 'serial',
        'unsigned' => true,
        'not null' => true,
      ),
      'host_name' => array(
        'description' => t('FileMaker host name.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => true,
      ),
      'database_name' => array(
        'description' => t('FileMaker database name.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => true,
      ),
      'user_name' => array(
        'description' => t('FileMaker username.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => true,
      ),
      'password' => array(
        'description' => t('FileMaker password.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => true,
      ),
      'port' => array(
        'description' => t('FileMaker port.'),
        'type' => 'int',
        'size' => 'small',
        'unsigned' => true,
        'not null' => true,
      ),
      'version' => array(
        'description' => t('FileMaker version.'),
        'type' => 'int',
        'size' => 'small',
        'unsigned' => true,
        'not null' => true,
      ),
      'https' => array(
        'type' => 'int', 
        'unsigned' => true, 
        'not null' => false, 
        'default' => 1, 
        'description' => t("Use HTTPS?")
      ),
    ),
    'primary key' => array('fmcid'),
  );

  $schema['filemaker_field'] = array(
    'description' => t('An instance of a FileMaker field. Attaches to a FileMaker webform node.'),
    'fields' => array(
      'fmfid' => array(
        'description' => t('Unique auto incrementing id.'),
        'type' => 'serial',
        'not null' => true,
      ),
      'nid' => array(
        'description' => t("The {filemaker}.nid"),
        'type' => 'int',
        'not null' => false,
      ),
      'name' => array(
        'description' => t("The FileMaker field's name"),
        'type' => 'varchar',
        'length' => '128',
        'not null' => false,
      ),
      'label' => array(
        'description' => t("The FileMaker field's label"),
        'type' => 'text',
        'not null' => false,
      ),
      'weight' => array(
        'description' => t("The FileMaker field's weight; used to determine sort order"),
        'type' => 'int',
        'not null' => true,
        'default' => 0,
      ),
      'type' => array(
        'description' => t('FileMaker field type, not MySQL or widget type.'),
        'type' => 'varchar',
        'length' => '20',
        'not null' => false,
      ),
      'widget' => array(
        'description' => t('Name of widget to use on webform. Used for the #type variable in the field array.'),
        'type' => 'varchar',
        'length' => '20',
        'not null' => false,
      ),
      'default_value' => array(
        'description' => t('Default value for the field.'),
        'type' => 'text',
        'not null' => false,
      ),
      'default_find_value' => array(
        'description' => t('Default value for the field in find mode.'),
        'type' => 'varchar',
        'length' => '20',
        'not null' => false,
      ),
      'default_create_value' => array(
        'description' => t('Default value for the field in create mode.'),
        'type' => 'varchar',
        'length' => '20',
        'not null' => false,
      ),
      'required' => array(
        'description' => t('Contains the number 1 if the field is required.'),
        'type' => 'int',
        'not null' => false,
      ),
      'include_field_in_create' => array(
        'type' => 'int', 
        'unsigned' => true, 
        'not null' => false, 
        'default' => 1, 
        'description' => t("The FileMaker field's \"Include field in create mode\" setting")
      ),
      'searchable' => array(
        'type' => 'int', 
        'unsigned' => true, 
        'not null' => false, 
        'default' => 0, 
        'description' => t("The FileMaker field's \"Searchable\" setting")
      ),
      'exact_search' => array(
        'type' => 'int', 
        'unsigned' => true, 
        'not null' => false, 
        'default' => 0, 
        'description' => t("The FileMaker field's \"Exact search\" setting")
      ),
      'include_field_in_search_results' => array(
        'type' => 'int', 
        'unsigned' => true, 
        'not null' => false, 
        'default' => 1, 
        'description' => t("The FileMaker field's \"Include field in search results\" setting")
      ),
    ),
    'primary key' => array('fmfid'),
  );
  
  return $schema;
}
